# `Lambda Calculus Evaluator`
# available at https://kdlcj.gitlab.io/lambda/

## This is a web application for evaluation of λ-calculus expressions.

### Features:
- reduction strategies: call by name, normal order, applicative order, call by value
- type systems: untyped, simply typed lambda calculus, Hindley-Milner typesystem
- import/export: file saving, loading, URL encoded terms, LaTeX export, custom print stylesheet
- aliasing support for terms, user-defined aliases are saved into file if desired 
- multiple pre-defined constants
- conversion to SKI combinatory calculus
- full syntactical form/shorthand form term-displaying

help page available at https://kdlcj.gitlab.io/lambda/help.html